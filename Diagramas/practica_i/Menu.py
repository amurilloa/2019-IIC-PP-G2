import practica_i.practica_uno as p

# Darle formato a los textos: https://pyformat.info/

print('>> Ejercicio 1 <<')
print('Cantidad Llantas 5:', p.ejercicio1(5))
print('Cantidad Llantas 4:', p.ejercicio1v2(4))

print('>> Ejercicio 2 <<')
print('Para su compra de 10000 tiene un descuento de ', p.ejercicio2(10000))


print('>> Ejercicio 3 <<')
print('f(4):', p.ejercicio3(4))
print('f(5):', p.ejercicio3(5))
print('f(6):', p.ejercicio3(6))
print('f(7):', p.ejercicio3(7))

print('>> Ejercicio 4 <<')
print('4 es par:', p.ejercicio4(4))
print('5 es par:', p.ejercicio4(5))
print('4 es un número', p.ejercicio4v2(4))
print('5 es un número', p.ejercicio4v2(5))

print('>> Ejercicio 5 <<')
print(p.ejercicio5(14))
print(p.ejercicio5(15))

print('>> Ejercicio 6 <<')
print(p.ejercicio6(5,25))
print(p.ejercicio6(5,23))
print(p.ejercicio6(5,27))

print('>> Ejercicio 7 <<')
print(p.ejercicio7(9, 6, 5, 4, 1))

print('>> Ejercicio 8 <<')
print(p.ejercicio8(9, 6, 5, 4, 1))

print('>> Ejercicio 9 <<')
print(p.ejercicio9(2,6,4,1,10))

print('>> Ejercicio 10 <<')
print(p.ejercicio10(100))
print(p.ejercicio10(101))

print('>> Ejercicio 11 <<')
print(p.ejercicio11(11000,2,0.5))

print('>> Ejercicio 12 <<')
print(p.ejercicio12(0,0,1,3,3,1,2,2,-1,-1))

print('>> Ejercicio 13 <<')
print(p.ejercicio13(1000,500,1539,4))

print('>> Ejercicio 14 <<')
print('Pulsaciones por cada 10seg de ejercicio, Mujer 25 años', p.ejercicio14('F',25))
print('Pulsaciones por cada 10seg de ejercicio, Hombre 25 años', p.ejercicio14('M',25))

print('>> Ejercicio 15 <<')
print('Crédito de ${0}, debe cancelar una cuota de ${1:.2f}'.format(700, p.ejercicio15(700)))
print('Crédito de ${0}, debe cancelar una cuota de ${1:.2f}'.format(499, p.ejercicio15(499)))

print('>> Ejercicio 16 <<')
print('Matricula promedio 69: ', p.ejercicio16(69, 20))
print('Matricula promedio 70: ', p.ejercicio16(70, 20))

print('>> Ejercicio 17 <<')
print('Capital 1000: ', p.ejercicio17(1000,4,5))

print('>> Ejercicio 18 <<')
print('4.9: ', p.ejercicio18(4.9))
print('6.9: ', p.ejercicio18(6.9))
print('8.5: ', p.ejercicio18(8.5))
print('9.9: ', p.ejercicio18(9.9))
print('10: ', p.ejercicio18(10))
