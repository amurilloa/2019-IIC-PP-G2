import math
import random


def ejercicio1(cantidad):
    if cantidad < 5:
        precio = 80
    else:
        precio = 70
    return precio


def ejercicio1v2(cantidad):
    if cantidad < 5:
        return 80
    else:
        return 70


def ejercicio2(total):
    n = random.randint(1, 100)
    if n < 50:
        return total * 0.15
    else:
        return total * 0.20


def ejercicio3(x):
    if x % 4 == 0:
        return x * x
    elif x % 4 == 1:
        return x / 6
    elif x % 4 == 2:
        return math.sqrt(x)
    else:  # elif x % 4 == 3
        return x * x * x + 5


def ejercicio4(num):
    return num % 2 == 0


def ejercicio4v2(num):
    return 'Par' if num % 2 == 0 else 'Impar'


def ejercicio4v3(num):
    return (True, 'Par') if num % 2 == 0 else (False, 'Impar')


def ejercicio5(num):
    n = num / 2
    if n % 2 == 1:
        return 'El número ' + str(num) + ' es el doble de un impar'
    else:
        return 'El número ' + str(num) + ' no es el doble de un impar'


def ejercicio6(n1, n2):
    num1 = math.pow(n1, 2)
    if num1 == n2:
        return '{0} es el cuadrado exacto de {1}    --> {1}² = {2:.0f}'.format(n2, n1, num1)
    elif n2 < num1:
        return '{0} es menor que el cuadrado de {1} --> {1}² = {2:.0f}'.format(n2, n1, num1)
    else:
        return '{0} es mayor que el cuadrado de {1} --> {1}² = {2:.0f}'.format(n2, n1, num1)


# TIPO_00CAMBIO = 582
# monto = 100000
# conv = monto / TIPO_CAMBIO
# print('₡', monto, 'son','$',round(conv,1))
# print('₡{0} son ${1:.1f}'.format(monto, conv))

def ejercicio7(n1, n2, n3, n4, n5):
    men = menor(n1, n2, n3, n4, n5)
    may = mayor(n1, n2, n3, n4, n5)
    return '{0}\n{1}'.format(men, may)


def menor(n1, n2, n3, n4, n5):
    men = n1
    if n2 < men:
        men = n2
    if n3 < men:
        men = n3
    if n4 < men:
        men = n4
    if n5 < men:
        men = n5
    return 'Menor: {0}'.format(men)


def mayor(n1, n2, n3, n4, n5):
    may = n1
    if n2 > may:
        may = n2
    if n3 > may:
        may = n3
    if n4 > may:
        may = n4
    if n5 > may:
        may = n5
    return 'Mayor: {0}'.format(may)


def ejercicio8(n1, n2, n3, n4, n5):
    return mayor(n1, n2, n3, n4, n5)


def ejercicio9(n1, n2, n3, n4, n5):
    cer = n2
    disCer = abs(n1 - n2)

    dis = abs(n1 - n3)
    if dis < disCer:
        disCer = dis
        cer = n3

    dis = abs(n1 - n4)
    if dis < disCer:
        disCer = dis
        cer = n4

    dis = abs(n1 - n5)
    if dis < disCer:
        disCer = dis
        cer = n5
    txt = 'espacio' if disCer == 1 else 'espacios'
    return 'El número más cercano a {0} es el {1} está a {2} {3}'.format(n1, cer, disCer, txt)


def ejercicio10(hect):
    HEC_A_MTS = 10000
    mts = hect * HEC_A_MTS

    if mts <= 1000000:
        pP = 0.45
        pE = 0.30
        pC = 0.25
    else:
        pP = 0.70
        pE = 0.20
        pC = 0.10

    cantP = mts * pP * 8 / 10
    cantE = mts * pE * 15 / 15
    cantC = mts * pC * 10 / 18

    return ('Distribución para {0} hectáreas\n'
            '- {1:>7} Pinos\n'
            '- {2:>7} Eucaliptos\n'
            '- {3:>7} Cedros').format(hect,
                                      math.trunc(cantP),
                                      math.trunc(cantE),
                                      math.trunc(cantC))


def ejercicio11(valor, pDep, pInc):
    dep = valor * pDep / 100 * 3
    inc = valor * pInc / 100 * 3
    print(dep, inc)
    if dep < inc / 2:
        return 'No compre el vehículo'
    else:
        return 'Compre el vehículo'


def distPuntos(x1, y1, x2, y2):
    res = math.sqrt(((x1 - x2) ** 2 + (y1 - y2) ** 2))
    return res


def ejercicio12(x1, y1, x2, y2, x3, y3, x4, y4, x5, y5):
    disCer = distPuntos(x1, y1, x2, y2)
    xCer = x2
    yCer = y2

    dis = distPuntos(x1, y1, x3, y3)
    if dis < disCer:
        disCer = dis
        xCer = x3
        yCer = y3

    dis = distPuntos(x1, y1, x4, y4)
    if dis < disCer:
        disCer = dis
        xCer = x4
        yCer = y4

    dis = distPuntos(x1, y1, x5, y5)
    if dis < disCer:
        disCer = dis
        xCer = x5
        yCer = y5

    return 'El punto más cercano a ({0},{1}) es el ({2},{3})'.format(x1, y1, xCer, yCer)


def ejercicio13(actual, monto, lim, por):
    nuevo = actual + monto + (por / 100 * actual)
    if nuevo > lim:
        return 'Su compra no puede ser procesada!!'
    else:
        return 'Compra realizada con éxito, nuevo balance ${0}'.format(nuevo)


def ejercicio14(sexo, edad):
    if sexo == 'F':
        return (220 - edad) / 10
    else:
        return (210 - edad) / 10


def ejercicio15(monto):
    if monto < 500:
        return monto * 0.03
    else:
        return monto * 0.02


def ejercicio16(pro, int):
    mat = 100
    if pro >= 70:
        return mat * 0.70
    else:
        return mat + int


def ejercicio17(cap, por, annos):
    if por > 0:
        return cap * (1 + por / 100) * annos
    else:
        return 'No se permiten porcentajes negativos'

def ejercicio18(nota):
    if nota == 10:
        return 'Matricula de Honor'
    elif nota > 8.5:
        return 'Sobresaliente'
    elif nota >= 7:
        return 'Notable'
    elif nota >= 5:
        return 'Suspenso'
    else:
        return 'Reprobado'

def ejercicio19(monto):
    detalle = ''

    dem = 500
    can = monto//dem
    monto = monto%dem
    if can > 0:
        detalle += '{0} billetes de ${1}\n'.format(can,dem)

    dem = 200
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle += '{0} billetes de ${1}\n'.format(can, dem)

    dem = 100
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle += '{0} billetes de ${1}\n'.format(can, dem)

    dem = 50
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle += '{0} billetes de ${1}\n'.format(can, dem)

    dem = 20
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle += '{0} billetes de ${1}\n'.format(can, dem)

    dem = 10
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle += '{0} billetes de ${1}\n'.format(can, dem)

    dem = 5
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle += '{0} billetes de ${1}\n'.format(can, dem)

    dem = 2
    can = monto // dem
    monto = monto % dem
    if can > 0:
        detalle += '{0} monedas de ${1}\n'.format(can, dem)

    if monto > 0:
        detalle += '{0} monedas de ${1}\n'.format(monto, 1)

    return detalle

print(ejercicio19(888))
