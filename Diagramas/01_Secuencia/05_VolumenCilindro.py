import math

print('Calculando volumen de un cilindro')

d = int(input('Diametro del Cilindro: '))
h = int(input('Altura del Cilindro: '))

r = d / 2

v = math.pi * math.pow(r, 2) * h

print('Volumen del cilindro:', round(v, 2))
