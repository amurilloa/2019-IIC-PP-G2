import math  # as m --> m.pow()

# variable + = + int( + input('mensaje') + )
a = int(input('Cateto 1: '))
b = int(input('Cateto 2: '))

s = math.pow(a, 2) + math.pow(b, 2)
# s = a ** 2 + b ** 2   --> ALTERNATIVA
h = math.sqrt(s)

print('Hipotenusa:', round(h,2))
# round(x,z) --> x el número a redondear y z la cantidad de decimales