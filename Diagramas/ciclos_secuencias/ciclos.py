# # while --> se usa cuando el ciclo tiende a ser infinito
# # for --> cantidad de iteraciones definida
#
# print('For con una lista (1,2,3,4,5)')
# for x in (1, 2, 3, 4, 5):
#     print(x, end=' ')
#
# print('\n\nFor con una lista (200,2,54,41,5)')
# for x in (200, 2, 54, 41, 5):
#     print(x, end=' ')
#
# print('\n\nFor con range con límite range(5)')
# for x in range(5):  # [0,5[
#     print(x, end=' ')
#
# print('\n\nFor con un range(10,20)')
# for x in range(10, 20):  # [10,20[
#     print(x, end=' ')
#
# print('\n\nFor con un range(0,20, 2), de 2 en 2')
# for x in range(0, 20, 2):  # [1,20[
#     print(x, end=' ')
#
# print('\n\nFor con un range(20,0, -1), invertido')
# for x in range(20, 0, -1):  # [1,20[
#     print(x, end=' ')
#
# print('\n')
# tabla = 5
# for x in range(1, 11):
#     if x % 2 != 0:
#         end = '\t'
#     else:
#         end = '\n'
#     print('{0}x{1}={2}'.format(tabla, x, tabla * x), end=end)
#
# # *
#
# # * * * * *
#
# # * * * * *
# # * * * * *
# # * * * * *
# # * * * * *
# # * * * * *
#
# # *
# # * *
# # * * *
# # * * * *
# # * * * * *
# # * * * * * *
# # * * * * * * *
# # * * * * * * * *
#
#
# print('*\n')
#
# for x in range(5):
#     print('*', end=' ')
#
# print('\n')
# for x in range(5):
#     print('* ' * 5)
#
# print('\n')
# for x in range(1, 9):
#     print('* ' * x)
#
#
# menu = ('Menu Prueba - UTN v0.1\n'
#         '1. Continuar\n'
#         '2. Salir\n'
#         'Seleccione una opción: ')
#
# salir = False
# while not salir:
#     op = int(input(menu))
#     if op == 1:
#         print('Continuar.....')
#     elif op == 2:
#         salir = True
#     else:
#         print('Opción inválida!!!')
#
# def esPar(num):
#     return num%2==0
#
# contP = 0
# num = 1
# pares = ''
# while contP < 1000000:
#     if esPar(num):
#         contP+=1
#         #print(num)
#         pares+='{0} '.format(num)
#     num+=1
# print(pares)

# cantidad de iteraciones
# 1 .. 10
for variable in (1,2,3,4,5,6):
    print('La variable es:', variable)

print()
for x in (32,4,2,6,1):
    print(x, end=' ')

print()
for x in range(10): #[0, 10[
    print(x, end=' ')

print()
numero = 54
cant = 0
while numero > 1:
    print(cant,'-->', numero)
    cant+=1
    if numero % 2 == 0:
        numero = numero / 2
    else:
        numero += 1

menu = ('Menu\n'
        '1. Hacer algo..\n'
        '2. Salir\n'
        'Seleccione una opción:')

while True:
    op = int(input(menu))
    if op == 1:
        print('Haciendo algo...')
    elif op == 2:
        print('Gracias por utilizar nuestra app')
        break
    else:
        print('Opción inválida!!')



total = 0
for x in range(1,11):
    total = total + x
print(total)

# 1+2+3+4+5+6+7+8+9+10=55












