txt = 'hola mundo'
lista = [1, 2, 3, 4, 5]
elistaMixta = ['asd', True, 123, 12.2]
tupla = (1, 2, 5, 4, 7, 8, 6)

# indexación positiva y negativa
# -10 -9 -8 -7 -6 -5 -4 -3 -2 -1
#   h  o  l  a     m  u  n  d  o
#   0  1  2  3  4  5  6  7  8  9

# elemento
print(txt[0])  # primer elemento
print(txt[-1])  # último elemento
print(txt[len(txt) - 1])  # último elemento en positivo

# membresía
print('h' in txt)  # Si existe o no el elemento en la secuencia

# Segmento/Trozo
print('1', txt[2:8])  # [2,8[
print('2', txt[2:])  # 2 hasta el final
print('3', txt[:5])  # inicio hasta el 5
print('4', txt)  # toda
print('5', txt[::-1])  # invierte la secuencia

# Min, Max, Largo
print('Min:', min(txt), '<---')
print('Max:', max(txt), '<---')
print('Largo:', len(txt))

# Recorrer una secuencia
for x in txt:
    print(x, end=' ')
print()

cont = 0
while cont < len(txt):
    print(txt[cont], end=' ')
    cont += 1
print()
for i in range(len(txt)):
    print(txt[i], end=' ')

# Mutabilidad - Mutable
# str --> textos --> no son mutables
# tuplas --> no son mutables
# listas --> son mutables
print()
print(lista)
lista[2] = 12
print(lista)


# buenos días
# Buenos días

def convertirMay(letra):
    abc = 'abcdefghijklmnñopqrstuvwxyz'
    ABC = 'ABCDEFGHIJKLMNÑOPQRSTUVWXYZ'
    for i in range(len(abc)):
        if abc[i] == letra:
            return ABC[i]
    return letra


txt = 'saludos'
nueva = convertirMay(txt[0])
print(nueva)
for x in txt[1:]:
    nueva += x
print(nueva)

# Métodos para strings
txt = 'hola mundo'

print(txt.capitalize())  # convertir a mayuscula la primera letra de una oración
txt = txt.capitalize()  # los metodos de los strings no modifican el valor original,
# tengo que hacer la asignacion
print(txt)

print('o:', txt.count('o'))  # cuantas apariciones de esa letra en el texto
print('Inicia con hol?:', 'Hola'.startswith('hol'))  # inicia con
print('Termina con a?:', 'Hola'.endswith('a'))  # termina con

if x in txt:
    print('index: x', txt.index('x'))
print('index:', txt.index('o'))  # busca la posición o indice de la letra
print('index:', txt.index('o', 2))
print('find: x', txt.find('x'))
print('find', txt.find('o'))
print('find', txt.find('o', 2))
print('aasdasdaóñ'.isalpha())  # si todos los elementos son letras
print('aasdasda123123óñ'.isalnum())  # si todos los elementos son alfanuméricos
print('2²'.isnumeric())  #
print('2\u00B2')
print('\u2211')
print('2323'.isdecimal())  # numero entre [0-9]
print('2²'.isdigit())

print('tExTo EsCrItO pOr ....'.lower())  # pasar a minúscula
print('tExTo EsCrItO pOr ....'.upper())  # pasar a mayúscula

print('asd'.islower())  # si está minúscula
print('ASD'.isupper())  # si está mayúscula

lista = 'arroz,frijoles,atun,cafe,chuletas,cerveza'
print(lista)
articulos = lista.split(',')  # separa por un delimitador
print(articulos)
print(''.join(articulos))  # une los elementos con un delimitador definido


# Crear un programa que solicite una cadena de caracteres y retorne al revés
# casa --> asac

def ejercicio100(frase):
    return frase[::-1]


def ejercicio101(frase):
    nueva = ''
    for x in frase:
        nueva = x + nueva
    return nueva


def ejercicio102(frase):
    nueva = ''
    u = len(frase) - 1
    for x in frase[0:u // 2 + 1]:
        nueva += x + frase[u]
        u -= 1
    if len(frase) % 2 != 0:
        nueva += '\b'
    return nueva


def ejercicio103(frase):
    lista = frase.split(' ')
    print(lista)
    nueva = ''.join(lista)
    print(nueva)
    esp = len(frase) - len(nueva)
    print(esp)
    return (frase, nueva, esp)


a, b, c = ejercicio103('ho la mun do')
print('Frase Original: {0}\nFrase resultante: {1}\nEspacios eliminados: {2}'.format(a, b, c))

# Ejercicio 3: solicitar una frase y eliminarle los espacios en blanco
# Mostrar al final: frase original, resultante y cantidad espacios eliminados
# "ho la mun do"
# "holamundo"
# 3 espacios eliminados
