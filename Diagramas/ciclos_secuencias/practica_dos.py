import math


# Pedir al usuario un número entre [0 y 10]
# 15 número inválido!!, solicitar otra vez
def ejercicio1(n, m):
    while True:
        num = int(input('Digite un número[{0}-{1}]: '.format(n, m)))
        if num >= n and num <= m:
            print(num, 'número válido!!')
            break
        elif num >= m and num <= n:
            print(num, 'número válido!!')
            break
        else:
            print(num, 'número inválido, favor intente nuevamente')


def leerInt(mensaje, limInf, limSup):
    pass


# ejercicio1(50,80)
# ejercicio1(80,50)

def ejercicio2():
    numeros = ''
    while True:
        num = int(input('Digite un número y un número negativo para salir: '))
        if num >= 0:
            numeros += '{0}, '.format(num)
        else:
            numeros += '\b\b'
            res = ('Números utilizados: {0}\n'
                   'Gracias por utilizar nuestra aplicación!!'.format(numeros))
            return res
            break


def ejercicio3():
    numeros = ''
    for x in range(6, 151):
        if x % 6 == 0:
            numeros += '{0}, '.format(x)
    return numeros + '\b\b'
    # for x in range(6,151, 6):
    #     print(x, end=' ')


def ejercicio5():
    cont = 0
    txt = ''
    while True:
        res = 2 ** cont
        if res >= 20 and res <= 230:
            txt += '2^{0}={1}\n'.format(cont, res)
        if res > 230:
            break
        cont = cont + 1
    return txt


# print(ejercicio5())


# 1+2+3+4+5+6+7+8+9+10=55
def ejercicio6(n, m):
    total = 0
    num = ''
    for i in range(n, m + 1):
        total = total + i
        num += '{0}+'.format(i)
    num += '\b'
    return '{0}={1}'.format(num, total)


# 1+2+3+4+5+6+7+8+9+10=55
# print(ejercicio6(10,1))
def ejercicio7(n, m):
    if n > m:
        return '1er número debe ser menor o igual que el 2ndo'
    total = 0
    num = ''
    for i in range(n, m + 1):
        total = total + i
        num += '{0}+'.format(i)
    num += '\b'
    return '{0}={1}'.format(num, total)


# print(ejercicio7(20,15))
def ejercicio8():  # definicion de la funcion
    txt = ''  # variable para guardar resultados
    for x in range(0, 201):  # ciclo que se repite 200 veces
        if x % 2 == 0:  # para saber si la variable por la que va el ciclo es par
            sl = '\n' if x % 20 == 0 else ' '  # un salto de linea cada multiplo de 20
            txt += '{0:3}{1}'.format(x, sl)  # guardando los resultados en una variable
    return txt  # termina el ciclo y retorna todos los resultados


def ejercicio9():
    txt = ''  # variable para guardar resultados
    for x in range(200, -1, -1):  # [200,-1[
        if x % 2 == 0:  # para saber si la variable por la que va el ciclo es par
            sl = '\n' if x % 20 == 0 else ' '  # un salto de linea cada multiplo de 20
            txt += '{0:3}{1}'.format(x, sl)  # guardando los resultados en una variable
    return txt  # termina el ciclo y retorna todos los resultados


# print(ejercicio9())

def ejercicio10(n):  # definicion de la funcion
    txt = ''  # variable para guardar resultados
    for x in range(0, n + 1):  # ciclo que se repite 200 veces
        if x % 2 == 0:  # para saber si la variable por la que va el ciclo es par
            sl = '\n' if x % 20 == 0 else ' '  # un salto de linea cada multiplo de 20
            txt += '{0:3}{1}'.format(x, sl)  # guardando los resultados en una variable
    return txt  # termina el ciclo y retorna todos los resultados


# print(ejercicio10(500))


def ejercicio11(n, m):
    if n > m:
        return '1er número debe ser menor o igual que el 2ndo'
    total = 0
    num = ''
    for i in range(n, m + 1):
        total = total + (i ** 2)
        num += '{0}\u00B2+'.format(i)
    num += '\b'
    return '{0}={1}'.format(num, total)


# print(ejercicio11(1,10))

def ejercicio12(n, m):
    if n > m:
        return '1er número debe ser menor o igual que el 2ndo'
    total = 0
    num = ''
    for i in range(n, m + 1):
        if i % 2 == 0:
            total = total + i
            num += '{0}+'.format(i)
    num += '\b'
    return '{0}={1}'.format(num, total)


# print(ejercicio12(1,10))

def ejercicio13():
    txt = ''
    for x in range(1, 101):
        sl = '\n' if x % 10 == 0 else ''
        txt += '{0:3} {1}'.format(x, sl)
    return txt


# print(ejercicio13())
def ejercicio14():
    print('Digite un negativo para salir')
    total = 0
    cant = 0
    while True:
        nota = int(input('Digite una nota: '))
        if nota < 0:
            break
        cant += 1
        total += nota
    return total / cant


# print(ejercicio14())

def ejercicio15(x, y):
    total = 1
    txt = ''
    for n in range(0, y):
        txt += '{0}*'.format(x)
        total *= x
    # txt = (str(x)+'*')*y
    txt += '\b'
    return '{0}={1}'.format(txt, total)


def ejercicio15v2(x, y):
    total = 1
    txt = ''
    while y > 0:
        total *= x
        txt += '{0}*'.format(x)
        y -= 1
    return '{0}={1}'.format(txt, total)


# print(ejercicio15v2(2,3))
# print(ejercicio15(2, 10))

# ejercicio20
def esPrimo(num):
    lim = math.trunc(math.sqrt(num))
    con = 2
    for div in range(2, lim + 1):
        if num % div == 0:
            con += 1
        if con > 2:
            return False
    return con <= 2


def ejercicio16():
    txt = ''
    for x in range(1, 1000000):
        if esPrimo(x):
            # print(x, end=' ')
            txt += '{0} '.format(x)
    return txt


# print(ejercicio16())


def esTerna(c1, c2, h):
    return c1 ** 2 + c2 ** 2 == h ** 2

def ejercicio17():
    txt = ''
    for c1 in range(1, 501):
        for c2 in range(1, 501):
            for h in range(1, 501):
                if esTerna(c1, c2, h):
                    txt+= '{0}²+{1}²={2}²\n'.format(c1, c2, h)

    return txt

#print(ejercicio17())

def ejercicio18(n,m):
    inicio = min(n,m)
    for x in range (inicio, 1, -1):
        if n%x==0 and m%x==0:
            return x
    return 1


def ejercicio19(a,b,c):
    x = min(a,b,c)
    while x > 0:
        if a%x==0 and b%x==0 and c%x==0:
            return x
        x-=1
    return 1

# print(ejercicio18(42,60))
# print(ejercicio19(30,60, 90))


