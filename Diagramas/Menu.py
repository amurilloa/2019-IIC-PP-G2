import  funciones.Funciones as f
#
# from funciones.Funciones import *
# # Interactuar con el usuario
# a = int(input('Valor #1: '))
# b = int(input('Valor #2: '))
# c = f.sumar(a, b)
# if f.esNegativo(c) and f.esPar(c):
#     print(a, '+', b, '=', c)
#
# # numero es negativo, true negativo, false: positivo
# n = int(input('Digite un número: '))
# print('¿Es negativo?', f.esNegativo(n))
#


menu = ("Funciones - UTN v0.1\n"
        "1. Sumar\n"
        "2. Restar\n"
        "3. Salir\n"
        "Seleccione una opción: ")

op = int(input(menu))
if op == 1:
    a = int(input('Valor #1: '))
    b = int(input('Valor #2: '))
    c = f.sumar(a, b)
    print(a, '+', b, '=', c)
elif op == 2:
    a = int(input('Valor #1: '))
    b = int(input('Valor #2: '))
    c = f.restar(a, b)
    print(a, '-', b, '=', c)