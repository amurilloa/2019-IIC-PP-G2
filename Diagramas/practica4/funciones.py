empleados = {}
generos = ['Masculino', 'Femenino']
estados = ['Solter', 'Casad', 'Divorciad', 'Viud']


def registrar(emp):
    ced = emp[0]
    empleados[ced] = emp


def impLista(lista, let=''):
    txt = ''
    con = 1
    for x in lista:
        txt += '{0}. {1}{2}\n'.format(con, x, let)
        con += 1
    return txt


def definirGenero(gen):
    let = 'a' if gen == 'Femenino' else 'o'
    return let


def impEmpleados():
    txt = ''
    con = 1
    for ced in empleados:
        emp = empleados[ced]
        txt += '{0:>4}. {1} | {2}\n      Datos de Contacto: {3}, {4}\n      Estado Civil: {5}\n'.format(con, emp[0],
                                                                                                        emp[1], emp[3],
                                                                                                        emp[2], emp[5])
        con += 1
    return txt


def existe(ced):
    if ced in empleados:
        return (True, empleados[ced])
    else:
        return (False, [])


def buscar(atributo):
    txt = ''
    con = 1
    for x in empleados:
        emp = empleados[x]
        if (atributo in empleados[x]
                or emp[1].lower().startswith(atributo.lower())
                or str(emp[0]) == atributo):
            txt += '{0:>4}. {1} | {2}\n      Datos de Contacto: {3}, {4}\n      Estado Civil: {5}\n'.format(con, emp[0],
                                                                                                            emp[1],
                                                                                                            emp[3],
                                                                                                            emp[2],
                                                                                                            emp[5])
            con += 1
    if txt == '':
        txt = 'No hay datos para ' + str(atributo)
    return txt


e = [206470762, 'Allan Murillo Alfaro', 'allanmual@gmail.com', '8526-2638', generos[0], 'Casado', '9826']
registrar(e)
e = [206470763, 'Alex Murillo Alfaro', 'alexmual@gmail.com', '8526-2639', generos[0], 'Soltero', '9725']
registrar(e)
