import practica4.funciones as log
import practica4.util as ut

menu = ('\nPractica 4 - UTN v0.1\n'
        '1. Registrar empleado\n'
        '2. Modificar un empleado\n'
        '3. Imprimir empleados(todos)\n'
        '4. Buscar empleados\n'
        '5. Salir\n'
        'Seleccione una opción: ')

despedida = 'Gracias por utilizar nuestra aplicación.'

while True:
    op = ut.leerInt(menu, 5)
    if op == 1:
        ced = ut.leerInt('Cédula o -1 cancelar: ')
        if ced == -1:
            continue;
        nom = ut.leerText('Nombre: ')
        cor = ut.leerText('Correo: ', False)
        tel = ut.leerFormato('Teléfono: ', '####-####')

        print('\nSeleccione un género:')
        print(log.impLista(log.generos), end='')
        pos = ut.leerInt('Género: ', len(log.generos))
        gen = log.generos[pos - 1]

        print('\nSeleccione su estado civil:')
        let = log.definirGenero(gen)
        print(log.impLista(log.estados, let), end='')
        pos = ut.leerInt('Estado Civil: ', len(log.estados))
        est = log.estados[pos - 1] + let
        pin = ut.leerFormato('Pin:', '####')
        e = [ced, nom, cor, tel, gen, est, pin]
        log.registrar(e)
    elif op == 2:
        print('Buscar la persona a modificar o -1 para cancelar: ')
        ced = ut.leerInt('Cédula: ')
        if ced == -1:
            continue;
        existe, emp = log.existe(ced)
        if existe:
            nom = ut.leerText('Nombre({0}): ', True, emp[1])
            cor = ut.leerText('Correo({0}): ', False, emp[2])
            tel = ut.leerFormato('Teléfono({0}): ', '####-####', emp[3])

            print('\nSeleccione un género:')
            print(log.impLista(log.generos), end='')
            pos = ut.leerInt('Género: ', len(log.generos))
            gen = log.generos[pos - 1]

            print('\nSeleccione su estado civil:')
            let = log.definirGenero(gen)
            print(log.impLista(log.estados, let), end='')
            pos = ut.leerInt('Estado Civil: ', len(log.estados))

            est = log.estados[pos - 1] + let
            pin = ut.leerFormato('Pin({0}):', '####', emp[6])
            e = [ced, nom, cor, tel, gen, est, pin]
            log.registrar(e)
        else:
            print('Usuario no registrado en el sistema.')
    elif op == 3:
        print(log.impEmpleados())
    elif op == 4:
        filtro = ut.leerText('¿Dígite la cédula, nombre, género o estado civil para buscar ?')
        print(log.buscar(filtro))
    else:
        print(despedida)
        break
