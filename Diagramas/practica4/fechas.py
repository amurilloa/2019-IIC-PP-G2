import datetime as dt
import practica4.util as ut


def leerFecha(msj):
    while True:
        try:
            txt = ut.leerText(msj)
            fecha = dt.datetime.strptime(txt, '%d/%m/%Y')
            return fecha
        except:
            print('Datos inválidos.', end=' ')

def impFecha(fecha):
    return  dt.date.strftime(fecha, '%a %d, %B %Y')


fecha1 = leerFecha('Fecha 1: ')
fecha2 = leerFecha('Fecha 2: ')
print(fecha1>fecha2)
print(fecha1)
print(impFecha(fecha1))
# Fecha 1: 28/02/2000
# Fecha 2: 01/03/2000
# https://docs.python.org/3.7/library/datetime.html#datetime.datetime.strptime


# import datetime as dt
#
# ya = dt.datetime.now()
# hoy = '{}/{}/{}'.format(ya.day,ya.month,ya.year)
# print(hoy)
# fecha = input('Fecha')
# consultarFecha(fecha)
