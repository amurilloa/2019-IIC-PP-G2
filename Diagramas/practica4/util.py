def leerInt(mensaje, max=-1):
    while True:
        try:
            num = int(input(mensaje))
            if num <= max or max == -1:
                return num
            else:
                print('\nOpción inválida.', end=' ')
        except ValueError:
            print('\nFavor digite un número.', end=' ')


def leerText(mensaje, req=True, ant=''):
    while True:
        txt = input(mensaje.format(ant)).strip()
        if len(txt) == 0:
            txt = ant
        if req and len(txt) > 0:
            return txt
        elif not req:
            return txt
        else:
            print('Favor digite un texto.', end=' ')


def leerFormato(mensaje, formato, ant=''):
    while True:
        txt = input(mensaje.format(ant)).strip()
        if len(txt) == 0:
            txt = ant
        if validar(formato, txt):
            return txt
        else:
            print('Favor digite un texto({0})'.format(formato), end=' ')


def validar(form, tele):
    if len(form) == len(tele):
        for ind in range(len(tele)):
            if form[ind] == '#' and not tele[ind].isdigit():
                return False
            elif form[ind] == 'C' and not tele[ind].isalpha():
                return False
            elif form[ind] == '-' and not tele[ind] == '-':
                return False
        return True
    else:
        return False
