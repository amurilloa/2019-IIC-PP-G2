"""
3.	Diseñe un diagrama de flujo que calcule e imprima el sueldo que le corresponde
al trabajador de una empresa que cobra ¢600.000 mensuales, el programa debe
realizar los cálculos en función de los siguientes criterios:

    a.	Si lleva más de 10 años en la empresa se le aplica un aumento del 20% anual.
    b.	Si lleva menos de 11 años, pero más que 5 se le aplica un aumento del 15% anual.
    c.	Si lleva menos de 6 años, pero más que 3 se le aplica un aumento del 13% anual.
    d.	Si lleva menos de 4 años se le aplica un aumento del 10% anual.

"""

a = int(input('Años Laborados: '))
SAL = 600000

if a < 4:
    p = 10
elif a < 6:
    p = 13
elif a < 11:
    p = 15
else:
    p = 20

a = p / 100 / 12 + 1 # 1.0166
sn = SAL * a
print(sn)