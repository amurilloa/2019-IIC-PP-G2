soc1 = int(input('Aporte #1: '))
soc2 = int(input('Aporte #2: '))
soc3 = int(input('Aporte #3: '))

total = soc1 + soc2 + soc3

psoc1 = soc1 * 100 / total
psoc2 = soc2 * 100 / total
psoc3 = soc3 * 100 / total

print('Aporte Socio #1', soc1, '(', round(psoc1, 1), '%)')
print('Aporte Socio #2', soc2, '(', round(psoc2, 1), '%)')
print('Aporte Socio #3', soc3, '(', round(psoc3, 1), '%)')
