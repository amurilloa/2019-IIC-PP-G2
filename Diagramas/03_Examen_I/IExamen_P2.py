import math

"""
Diseñe un diagrama de flujo que permita calcular e imprimir la circunferencia
de un círculo, el perímetro de un rectángulo y el área de un rombo según la
opción seleccionada por el usuario. No debe pedir datos que no va a usar en
el proceso (ejemplo: no pedir base, si el usuario quiere el área del circulo)
"""

o = int(input('I Examen - Pregunta 2\n'
              '1. Circulo\n'
              '2. Perimetro\n'
              '3. Rombo\n'
              'Seleccione una opción: '))

if o == 1:
    d = int(input('Diametro del Círculo: '))
    r = d / 2
    cir = 2 * math.pi * r
    print('Circunferencia: ', cir)
elif o == 2:
    l = int(input('Largo: '))
    a = int(input('Ancho: '))
    p = 2 * l + 2 * a
    print('Perímetro: ', p)
else:
    dmen = int(input('Diagonal Menor: '))
    dmay = int(input('Diagonal Mayor: '))
    a = dmen * dmay / 2
    print('Área del Rombo: ', a)
