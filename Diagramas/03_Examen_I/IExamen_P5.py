hrs = int(input('Horas: '))
min = int(input('Minutos: '))
seg = int(input('Segundos: '))
seg = seg - 1  # seg -= 1

if seg == -1:
    seg = 59
    min = min - 1
    if min == -1:
        min = 59
        hrs = hrs - 1
        if hrs == 0:
            hrs = 12

print(hrs, ':', min, ':', seg)
