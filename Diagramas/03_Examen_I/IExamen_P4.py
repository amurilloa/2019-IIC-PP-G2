nom = input('Nombre: ')
hrs = int(input('Horas: '))
pre = int(input('Precio: '))

# Salario Bruto
if hrs <= 40:
    sb = hrs * pre
else:
    ext = hrs - 40
    sb = 40 * pre + ext * pre * 1.5
    print(40 * pre)
    print(ext * pre * 1.5)

# Salario Neto
if sb <= 200000:
    sn = sb
    i = 0
elif sb < 400000:
    e = sb - 200000
    i = e * 0.10
    sn = sb - i
else:
    e = sb - 400000
    i = e * 0.15 + 200000 * 0.10
    sn = sb - i

# Salida
print('Nombre:', nom)
print('Salario Bruto:', sb)
print('Salario Neto:', sn)
print('Impuestos:', i)


