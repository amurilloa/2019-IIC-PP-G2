
def sumar(n1, n2):  # encabezado: def + nombre + (parámetros):
    res = n1 + n2  # hace la funcion
    return res  # que quiero retornar

def esPar(n1):
    return n1 % 2 == 0

def esNegativo(numero):
    return numero < 0
    # if numero < 0:
    #     return True
    # else:
    #     return False

def restar(n1, n2):
    res = n1 - n2
    return res
