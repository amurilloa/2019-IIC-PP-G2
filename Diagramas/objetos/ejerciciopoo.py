# Dispensadora de cafe
# Bebida, Maquina
class Bebida:
    def __init__(self, nom, pre=1000, tam='P'):
        self.nombre = nom
        self.precio = pre
        self.tamano = tam

    def __str__(self):
        tam = 'Peq.' if self.tamano == 'P' else 'Med.' if self.tamano == 'M' else 'Gra.'
        return '{0:12}({1}-₡{2})'.format(self.nombre, tam, self.precio)


class Maquina:
    def __init__(self, mar='', neg=''):
        self.marca = mar
        self.negocio = neg
        self.bebidas = []

    def add_bebida(self, bebida):
        self.bebidas.append(bebida)

    def comprar(self, num, monto):
        bed = self.bebidas[num-1]
        vuelto = monto - bed.precio
        if vuelto >= 0:
            return 'Gracias por su compra', bed, vuelto
        else:
            return 'Fondos insuficientes', '', monto

    def __str__(self):
        txt = '\n{0} - {1}\nLista de Bebidas:\n'.format(self.negocio, self.marca)
        con = 1
        for b in self.bebidas:
            end = '\n' if con % 2 == 0 else '\t\t'
            txt += ' {0}-{1}{2}'.format(con, b, end)
            con += 1
        return txt


b1 = Bebida('Moka')
m1 = Maquina('Nescafe', 'Super Santa Rita')
m1.add_bebida(Bebida('Cafe Negro'))
m1.add_bebida(Bebida('Cafe Negro', 1500, 'M'))
m1.add_bebida(Bebida('Capucchino'))
m1.add_bebida(Bebida('Capucchino', 1500, 'M'))
m1.add_bebida(b1)
m1.add_bebida(Bebida('Moka', 1500, 'M'))
m1.add_bebida(Bebida('Mokacchino'))
m1.add_bebida(Bebida('Mokacchino', 1750, 'M'))
m1.add_bebida(Bebida('Latte'))
m1.add_bebida(Bebida('Chocolate'))



