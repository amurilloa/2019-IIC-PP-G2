class Perro: # Singular y Mayuscula la primera letra
    def __init__(self, nom='', col=''):  # Atributos
        self.nombre = nom
        self.color = col
        self.trucos = []
    # Comportamientos, métodos, funciones
    def agregar_truco(self, truco):
        self.trucos.append(truco)

    def ladrar(self):
        return 'Guau guau...'

    def __str__(self): # Este método va indicar como imprimir el objeto
        txt = 'Trucos:\n'
        for x in self.trucos:
            txt += ' -{0}\n'.format(x)
        return '{0}({1})\n{2}'.format(self.nombre, self.color, txt)
p1 = Perro('Capitán')
p1.agregar_truco('Rodar')
p1.agregar_truco('Hacerse el muerto')
print(p1.ladrar())
print(p1)
# Objeto, 6 atributos, __init__, __str__, 1 método