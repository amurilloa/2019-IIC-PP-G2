def areaTriangulo(base, altura):
    a = base * altura / 2
    return a

def compararNumeros(n1, n2):
    """
    :param n1:
    :param n2:
    :return:
    """
    if n1 > n2:
        return 'es mayor'
    elif n2 > n1:
        return 'es menor'
    else:
        return 'es igual'
