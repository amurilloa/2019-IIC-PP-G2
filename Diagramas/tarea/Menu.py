import tarea.Funciones as f

menu = ('Funciones - UTN v0.1\n'
        '1. Área Triángulo\n'
        '2. Hipotenusa\n'
        '7. Comparar Números\n'
        '22. Salir\n'
        'Seleccione una opción: ')
op = int(input())
if op == 1:
    b = int(input("Base: "))
    a = int(input("Altura: "))
    area = f.areaTriangulo(b, a)
    print('Área del Triángulo: ', area)
if op == 7:
    n1 = int(input('#1: '))
    n2 = int(input('#2: '))
    res = f.compararNumeros(n1, n2)
    print(n1, res, n2)
elif op == 22:
    print('Gracias por utilzar la aplicación')