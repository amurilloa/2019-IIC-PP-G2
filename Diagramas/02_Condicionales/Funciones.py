import math


# def + nombre + ( + parametros* + )+ :
#   Lo que hace la función
# EVITAR: Pedir datos e imprimir..... .---> menu
def areaRectangulo(base, altura):
    """
    Area de un rectangulo
    :param base: int base del rectángulo
    :param altura: int altura del rectángulo
    :return: int área del rectángulo
    """
    area = base * altura
    return area


def areaTriangulo(base, altura):
    area = base * altura / 2
    return area


def areaCuadrado(lado):
    area = lado * lado
    return area


# Menu - Interacción con el usuario
menu = ('Ejemplo de Menu - UTN v0.1\n'
        '1. Área de Rectángulo\n'
        '2. Área de Triángulo\n'
        '3. Área de Cuadrado\n'
        '4. Salir\n'
        'Seleccione una opción: ')

op = input(menu)

# if op == 1:
