import datetime

diccionario = {}


def impRazas():
    txt = ''
    con = 1
    for x in razas:
        txt += '{0}. {1}\n'.format(con, x)
        con += 1
    return txt


razas = ['Pastor Alemán', 'Pastor Australiano', 'French Poodle', 'Labrador', 'Otro']

conMasc = 1  # Variable Global


def buscarMascotas(filtro):  # usuario, pass
    txt = ''
    cont = 1
    for x in diccionario:
        mascota = diccionario[x]
        if filtro in mascota:  # [genCodMas(), 'Chuck', 'Pastor Australiano', 'Negro', '25/06/2018', False, 'Jared']
            txt += '{0}. {1} - {2}({3})\n'.format(cont, mascota[0], mascota[1], mascota[2])
            cont += 1
    if len(txt) == 0:
        txt = 'No hay datos para ' + filtro
    return txt


def registrar(mascota):
    cod = mascota[0]
    diccionario[cod] = mascota


def leerInt(mensaje, max=-1):
    while True:
        try:
            num = int(input(mensaje))
            if num <= max or max == -1:
                return num
            else:
                print('\nOpción inválida.', end=' ')
        except ValueError:
            print('\nFavor digite un número.', end=' ')


def pregunta(preg):
    """
    Para que sirve el método
    :param preg: que es el parámetro ?
    :return: Que retorna el método
    """
    while True:
        res = input(preg + '\n -SI\n -NO\n Digite una opción: ')
        res = res.lower()
        if len(res) > 0:
            if res[0] == 's' or res[0] == 'y':
                return True
            elif res[0] == 'n':
                return False
            else:
                print('Opción inválida.')
        else:
            print('Favor digite lo solicitado.')


def genCodMas():
    global conMasc
    fecha = datetime.datetime.now()  # variable local
    anno = fecha.year
    cod = anno * 1000 + conMasc
    conMasc += 1
    return cod


# Datos prueba
cod = genCodMas()
mascota = [cod, 'Capitán', 'Pastor Alemán', 'Cafe', '27/06/2017', True, 'Allan Murillo Alfaro']
registrar(mascota)
mascota = [genCodMas(), 'Firulais', 'Pastor Australiano', 'Negro', '25/06/2018', False, 'Desconocido']
registrar(mascota)
mascota = [genCodMas(), 'Chuck', 'Pastor Australiano', 'Negro', '25/06/2018', False, 'Jared']
registrar(mascota)
mascota = [genCodMas(), 'Candy', 'French Poodle', 'Negro', '25/06/2018', False, 'Jared']
registrar(mascota)

lista = []

ced = 0
nom = 1
ape = 2

per = {206470762: [206470762, 'Allan', 'Murillo'],
       901110296: [901110296, 'Mario', 'Corrales']}
#
# print(per)
# ced = 901110296  # int(input('Cédula a modificar: '))
# print(per[ced])
# nom = 'Mario Alberto'#input('Nombre: ')
# ape = 'Corrales'#input('Apellido: ')
# obj = [ced, nom, ape]
# print(obj)
# per[ced] = obj
# print(per)
#
# per[801110296] = 'patito'
# print(per)


#
#
#
#
#
#
# if 206470762 in diccci:
#     persona = diccci[206470762]
#     print(persona[ced], persona[nom])
#
# marcas = {'Toyota':['Corolla', 'Prado','Yaris'],
#           'Nissan':['Sentra', 'X-Trail']}
#
# def impLista(lista):
#     txt = ''
#     con = 1
#     for x in lista:
#         txt += '{0}. {1}\n'.format(con, x)
#         con += 1
#     return txt
#
# print(marcas.keys())
# print(impLista(marcas.keys()))
#
# print(impLista(marcas['Toyota']))

placa = 'Bmx-432'.upper()
print(placa)

dic = {21212: [21212, 'Candy', 'French Poodle', 'Negro', '25/06/2018', False, 'Jared', 'M']}


def login(ced, con):
    if ced in dic:
        usu = dic[ced]
        if usu[6] == con:
            return True, usu
    return False, []


entro, usuario = login(21212, 'Jared')
print(entro, usuario)
if usuario[7] == 'S':
    print('Secretaria')
    # while True:
    #     pass
elif usuario[7] == 'M':
    print('Médico')
else:
    print('Error ')
