# lista = [1,2,3,4]
# lista.append(6) agrega un elemento
# lista.remove(2) elimina un elemento

# dict = {13: 'hola',
#         22: 'perro',
#         36: True,
#         47: 324,
#         42: 43534,
#         52: [1,2,3,4,5,6]}

# dict[2019001] = [2019001, 'Capitán', 'Pastor Alemán', 'Cafe', '28/06/2016', True, 'Allan Murillo Alfaro']
#
# lista = dict[2019001]
# print(lista[0],'-',lista[1])
# print(dict)
#
# if 2019001 in dict:
#     print(dict[2019001])