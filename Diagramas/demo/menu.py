# Mascotas - UTN v0.1
# 1. Mascotas
# 2. Salir

# 1. Registrar Mascotas
# 2. Buscar Mascotas
# 3. Volver

#  Cod, Nombre, Raza, Color, Fecha Nacimiento, Pedigree, Dueño

# 1. Buscar por dueño
# 2. Buscar por raza
# 3. Cancelar

import demo.logica as log

menuP = ('\nMascotas - UTN v0.1\n'
         '1. Mascotas\n'
         '2. Salir\n'
         'Seleccione una opción: ')

menuM = ('\nMascotas - UTN v0.1\n'
         '1. Registrar Mascotas\n'
         '2. Buscar Mascotas\n'
         '3. Volver\n'
         'Seleccione una opción: ')

menuB = ('\nMascotas - UTN v0.1\n'
         '1. Por dueño\n'
         '2. Por raza\n'
         '3. Cancelar\n'
         'Seleccione una opción: ')

while True:
    op = log.leerInt(menuP, 2)
    if op == 1:  # Mascotas
        while True:
            op = log.leerInt(menuM, 3)
            if op == 1:  # Registrar mascotas
                cod = int(input('Código:')) #log.genCodMas()
                nom = input('Nombre: ')
                print(log.impRazas())
                num = log.leerInt('Seleccione una raza: ', len(log.razas))
                raz = log.razas[num-1]
                col = input('Color: ')
                fec = input('Fecha de Nacimiento(dd/mm/aaaa): ')
                ped = log.pregunta('¿Tiene pedigree?')
                due = input('Dueño: ')
                mascota = [cod, nom, raz, col, fec, ped, due]
                log.registrar(mascota)
            elif op == 2:  # Buscar Mascotas
                while True:
                    op = log.leerInt(menuB, 3)
                    if op == 1:
                        nom = input('Dueño: ')
                        print(log.buscarMascotas(nom))
                    elif op == 2:
                        print(log.impRazas())
                        num = log.leerInt('Seleccione una raza: ', len(log.razas))
                        raz = log.razas[num - 1]
                        print(log.buscarMascotas(raz))
                    else:
                        break
            else:  # Volver
                break
    else:  # Salir
        print('Gracias por utilizar la aplicación')
        break

