import math

def areaRect(ancho, alto):
    """
    Calcula el area de un rectangulo, redondeada al entero siguiente
    :param ancho: ancho del rectangulo
    :param alto: alto del rectangulo
    :return: area del rectangulo redondeada al entero siguiente
    """
    area = ancho * alto
    return round(area + 0.5)


def areaCir(diametro):
    """
    Calcula el area de un circulo, redondeando al entero siguiente
    :param diametro: diametro del circulo
    :return: area del circulo redondeado al entero siguiente
    """
    r = diametro / 2
    area = math.pi * math.pow(r, 2)
    return round(area + 0.5)


# Menu
COS_X_GAL = 20
GAL_X_M2 = 10

pregP = ('¿Quiere agregar una pared?\n1.SI\n2.NO\nSeleccione una opción: ')
pregV = ('¿Desea agregar ventanas ?\n'
         '1. SI, Ventana Rectagular\n'
         '2. SI, Ventana Circular\n'
         '3. NO\n'
         'Seleccione una opción: ')

print('Pintor - UTN v0.1\nMedidas de la Pared')

areaP = 0
areaV = 0
while True:
    op = int(input(pregP))
    if op == 1:
        an = float(input('Ancho: '))
        al = float(input('Alto: '))
        areaP += areaRect(an, al)
        while True:
            op = int(input(pregV))
            if op == 1:
                print('Pintor - UTN v0.1\nMedidas de la Ventana')
                an = float(input('Ancho: '))
                al = float(input('Alto: '))
                a = areaRect(an, al)
                if a < areaP:
                    areaV += a
                else:
                    print('El área de la ventana no puede ser mayor al área de la pared')
            elif op == 2:
                print('Pintor - UTN v0.1\nMedidas de la Ventana')
                d = float(input('Diametro: '))
                a = areaCir(d)
                if a < areaP:
                    areaV += a
                else:
                    print('El área de la ventana no puede ser mayor al área de la pared')
            else:
                break
    else:
        break
print('Total paredes:', areaP, 'm²')
print('Total de ventanas:', areaV, 'm²')
total = areaP - areaV
print('Total a pintar:', total, 'm²')
print('Costo por galón aplicado: $', COS_X_GAL, ' (1 Galón por', GAL_X_M2, 'm²)')
cant = round(total / GAL_X_M2 + 0.5)
costo = cant * COS_X_GAL
print('Presupuesto: ', cant, 'gal x $', COS_X_GAL, '= $', costo)
print('')
